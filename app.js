// Setup the modules/dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoutes = require("./routes/taskRoute.js");

// Setup server
const app = express();
const port = 4001;

// [Middleware]
// Convert files into JSON
app.use(express.json());
// Allow express to read all the urlencoded
app.use(express.urlencoded({ extended: true }));
// Add the tasks routes
// Allow all the task routes created in the taskRoutes.js file to use/
app.use("/tasks", taskRoutes);

//Database connection (mongodb)
mongoose.connect(
    "mongodb+srv://admin:admin1234@cluster0.tt3qfyy.mongodb.net/B229_to-do?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }
);

// Set Notification for connection success or failure
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.on("open", () => console.log("We are connected to cloud database"));

// Server Listening
app.listen(port, () => console.log(`Now listening on ${port}`));
