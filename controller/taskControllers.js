const Task = require("../models/task.js");

module.exports.getAllTasks = () => {
    return Task.find({}).then((results) => {
        return results;
    });
};

module.exports.createTask = (requestBody) => {
    let newTask = new Task({
        name: requestBody.name,
    });

    return newTask.save().then((task, error) => {
        if (error) {
            console.log(error);
            return false;
        } else {
            return task;
        }
    });
};

module.exports.deleteTask = (taskId) => {
    return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
        if (error) {
            console.log(error);
            return false;
        } else {
            return removedTask;
        }
    });
};

module.exports.updateTask = (taskId, newContent) => {
    return Task.findById(taskId).then((result, error) => {
        if (error) {
            console.log(error);
            return false;
        }
        result.name = newContent.name;

        return result.save().then((updatedtask, error) => {
            if (error) {
                console.log(error);
            } else {
                return updatedtask;
            }
        });
    });
};

module.exports.getTask = (taskId) => {
    return Task.findById(taskId).then((result, error) => {
        if (error) {
            console.log(error);
            return false;
        } else {
            return result;
        }
    });
};

module.exports.completeTask = (taskId) => {
    return Task.findById(taskId).then((result, error) => {
        if (error) {
            console.log(error);
            return false;
        }
        result.status = "complete";

        return result.save().then((updatedTask, error) => {
            if (error) {
                console.log(error);
                return false;
            } else {
                return updatedTask;
            }
        });
    });
};
